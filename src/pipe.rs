use bracket_lib::prelude::*;
pub enum PipePosition {
    Top,
    Bottom,
    Both,
    None,
}
pub struct Pipe {
    x: f32,
    position: PipePosition,
    height: i32,
}

impl Pipe {
    fn new(x: f32, position: PipePosition, height: i32) -> Self {
        Pipe {
            x,
            position,
            height,
        }
    }

    pub fn x(&self) -> &f32 {
        &self.x
    }

    pub fn position(&self) -> &PipePosition {
        &self.position
    }

    pub fn height(&self) -> &i32 {
        &self.height
    }

    fn render(&mut self, context: &mut BTerm) {
        match self.position {
            PipePosition::Top => {
                self.render_top(context);
            }
            PipePosition::Bottom => {
                self.render_bottom(context);
            }
            PipePosition::Both => {
                self.render_top(context);
                self.render_bottom(context);
            }
            PipePosition::None => {}
        }
    }

    fn update(&mut self) {
        self.x += -1.0;
    }

    fn is_offscreen(& self) -> bool{
        self.x < -3.0
    }

    fn render_top(&mut self, context: &mut BTerm) {
        for y in 0..self.height {
            context.set(self.x as i32, y, GREEN, BLACK, 178);
            context.set(self.x as i32 + 1, y, GREEN, BLACK, 178);
            context.set(self.x as i32 + 2, y, GREEN, BLACK, 178);
        }
    }

    fn render_bottom(&mut self, context: &mut BTerm) {
        let start = 100 - self.height;
        for y in start..100 {
            context.set(self.x as i32, y, GREEN, BLACK, 178);
            context.set(self.x as i32 + 1, y, GREEN, BLACK, 178);
            context.set(self.x as i32 + 2, y, GREEN, BLACK, 178);
        }
    }
}

pub struct PipeManager {
    upcoming: Vec<UpcomingPipe>,
    pipes: Vec<Pipe>,
    update_count: i32,
}

impl PipeManager {
    pub fn new() -> Self {
        let data = include_str!("./resources/pipes.txt");
        let upcoming: Vec<UpcomingPipe> = data.lines().rev().map(build_upcoming_pipe).collect();
        PipeManager {
            upcoming,
            pipes: vec![],
            update_count: 0,
        }
    }

    pub fn update(&mut self) {
        self.pipes.iter_mut().for_each(|pipe| pipe.update());
        self.update_count += 1;
        if self.update_count % 10 == 0 {
            match self.upcoming.pop() {
                Some(new_pipe) => {
                    let pipe = Pipe::new(100.0, new_pipe.position, new_pipe.height);
                    self.pipes.push(pipe);
                }
                _ => {}
            };
        }
        self.pipes.retain(|pipe| !pipe.is_offscreen());
    }

    pub fn render(&mut self, context: &mut BTerm) {
        self.pipes.iter_mut().for_each(|pipe| pipe.render(context));
    }

    pub fn pipes(&self) -> &Vec<Pipe> {
        &self.pipes
    }
}

struct UpcomingPipe {
    position: PipePosition,
    height: i32,
}

fn build_upcoming_pipe(line: &str) -> UpcomingPipe {
    let mut items = line.split(',');
    let letter = items.next().unwrap_or("N");
    let position = match letter {
        "T" => PipePosition::Top,
        "B" => PipePosition::Bottom,
        "A" => PipePosition::Both,
        _ => PipePosition::None,
    };
    let height = i32::from_str_radix(items.next().unwrap_or("0"), 10).unwrap_or(0);
    UpcomingPipe { position, height }
}
