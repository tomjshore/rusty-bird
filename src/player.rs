use crate::pipe;
use bracket_lib::prelude::*;

pub struct Player {
    x: f32,
    y: f32,
    velocity: f32,
}
impl Player {
    pub fn new(x: f32, y: f32) -> Self {
        Player {
            x,
            y,
            velocity: 0.0,
        }
    }

    pub fn flap(&mut self) {
        self.velocity += -0.5;
    }

    pub fn render(&mut self, context: &mut BTerm) {
        context.set(self.x as i32, self.y as i32, YELLOW, BLACK, 1);
    }

    pub fn update(&mut self) {
        if self.velocity < 2.0 {
            self.velocity += 0.15;
        }
        self.y += self.velocity;
        if self.y < 0.0 {
            self.y = 0.0;
            self.velocity = 0.0;
        }
    }

    pub fn is_dead(&mut self, screen_height: i32, pipe_manager: &pipe::PipeManager) -> bool {
        if self.y as i32 > screen_height {
            return true;
        }

        if pipe_manager
            .pipes()
            .iter()
            .any(|pipe| self.colide_with_pipe(pipe))
        {
            return true;
        }

        false
    }

    fn colide_with_pipe(&mut self, pipe: &pipe::Pipe) -> bool {
        if self.x > *pipe.x() && self.x < *pipe.x() + 3.0 {
            return match *pipe.position() {
                pipe::PipePosition::Top => self.colide_with_top_pipe(pipe),
                pipe::PipePosition::Bottom => self.colide_with_bottom_pipe(pipe),
                pipe::PipePosition::Both => {
                    if self.colide_with_top_pipe(pipe) {
                        return true;
                    }
                    return self.colide_with_bottom_pipe(pipe);
                }
                pipe::PipePosition::None => false,
            };
        }
        false
    }
    fn colide_with_top_pipe(&mut self, pipe: &pipe::Pipe) -> bool {
        (self.y as i32) < *pipe.height()
    }
    fn colide_with_bottom_pipe(&mut self, pipe: &pipe::Pipe) -> bool {
        (self.y as i32) > 100 - *pipe.height()
    }
}
