use bracket_lib::prelude::*;
mod pipe;
mod player;

const SCREEN_WIDTH: i32 = 100;
const SCREEN_HEIGHT: i32 = 100;
const FRAME_DURATION: f32 = 75.0;

fn main() -> BError {
    let context = BTermBuilder::simple(SCREEN_WIDTH, SCREEN_HEIGHT)?
        .with_fps_cap(60.0)
        .with_title("Uleska Ascii Bird")
        .build()?;
    main_loop(context, State::new())
}

struct State {
    mode: GameMode,
    player: player::Player,
    frame_time: f32,
    pipe_manager: pipe::PipeManager,
    score : u32,
}

impl State {
    fn new() -> Self {
        State {
            mode: GameMode::Menu,
            player: player::Player::new(5.0, SCREEN_WIDTH as f32 / 2.0),
            frame_time: 0.0,
            pipe_manager: pipe::PipeManager::new(),
            score: 0,
        }
    }

    fn start(&mut self) {
        self.mode = GameMode::Playing;
        self.player = player::Player::new(5.0, SCREEN_WIDTH as f32 / 2.0);
        self.pipe_manager = pipe::PipeManager::new();
        self.score = 0;
    }

    fn main_menu(&mut self, context: &mut BTerm) {
        context.print_centered(5, "Rusty Bird!!!");
        context.print_centered(6, "--------------------");
        context.print_centered(9, "(P) Play");
        context.print_centered(10, "(Q) Quit");

        if let Some(key) = context.key {
            match key {
                VirtualKeyCode::P => self.start(),
                VirtualKeyCode::Q => context.quitting = true,
                _ => {}
            }
        }
    }
    fn dead(&mut self, context: &mut BTerm) {
        context.print_centered(5, "  You'r Dead loser ");
        context.print_centered(6, "--------------------");
        context.print_centered(9, "(P) Again");
        context.print_centered(10, "(Q) Menu");
        context.print_centered(15, format!("You scored {}", self.score));

        if let Some(key) = context.key {
            match key {
                VirtualKeyCode::P => self.start(),
                VirtualKeyCode::Q => self.mode = GameMode::Menu,
                _ => {}
            }
        }
    }
    fn playing(&mut self, context: &mut BTerm) {
        self.frame_time += context.frame_time_ms;
        if self.frame_time > FRAME_DURATION {
            self.frame_time = 0.0;
            self.player.update();
            self.pipe_manager.update();
            self.score += 1;
        }
        if let Some(VirtualKeyCode::Space) = context.key {
            self.player.flap();
        }
        self.player.render(context);
        self.pipe_manager.render(context);
        context.print(80,99, format!("Score : {}", self.score));
        if self.player.is_dead(SCREEN_HEIGHT, &self.pipe_manager) {
            self.mode = GameMode::End;
        }
    }
}

impl GameState for State {
    fn tick(&mut self, context: &mut BTerm) {
        context.cls_bg(NAVY);
        match self.mode {
            GameMode::Menu => self.main_menu(context),
            GameMode::End => self.dead(context),
            GameMode::Playing => self.playing(context),
        }
        context.with_post_scanlines(true);
    }
}

enum GameMode {
    Menu,
    Playing,
    End,
}
