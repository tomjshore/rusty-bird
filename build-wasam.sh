#!/bin/bash

cargo build --release --target wasm32-unknown-unknown
rm wasm/flappy*
wasm-bindgen target/wasm32-unknown-unknown/release/flappy.wasm --out-dir wasm --no-modules --no-typescript
